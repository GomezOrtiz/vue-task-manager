module.exports = {
    ci: {
        collect: {
            url: "https://tasks.gomezortiz.com/",
            additive: true,
            settings: {
                chromeFlags: "--no-sandbox"
            }
        }
    }
}