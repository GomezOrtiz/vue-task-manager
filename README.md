# Vue Task Manager

Simple task manager developed to experiment with Vue, Vuex, Jest, Gitlab CI, Firebase and Google Kubernetes Engine

### AVAILABLE COMMANDS

All available commands are listed in Makefile.

### CI/CD

- Pushing a commit to any branch will trigger pipeline's test stage, which will run all the application tests
- Pushing a commit to STG branch will trigger development CI/CD pipeline, which will deploy to Firebase with this URL: https://vue-crud-4cc34.web.app/
- Increasing version (see the following section) will trigger production CI/CD pipeline, which will push a tagged Docker image, update :latest image and deploy to Google Kubernetes Engine with this URL: https://tasks.gomezortiz.com/

### Versioning

We use SemVer (https://semver.org/) versioning standard. According to this convention, each release tag should have three numbers separated by a dot, and each number has its own meaning: 

The format for this standard is **Major.Minor.Patch**. 

- We change the patch version whenever there is a fix in the release, for example, a bug. 
- We change the minor version whenever there are changes in the release but with backward compatibility, for example, when we add methods or some more additional functionality. 
- We change the major version when there are breaking changes without backward compatibility, for example, when we change the existing API or endpoints.

To manage versions, we use the following NPM built in commands (https://docs.npmjs.com/cli/version), made available by Make aliases:

- **make deploy-patch** -> deploys a new version incrementing patch number
- **make deploy-minor** -> creates a new version incrementing minor number
- **make deploy-major** -> creates a new version incrementing major number

Each of these commands also pushes a git commit with the package.json changed and pushes a git tag annotated with the new version number. While pushing the tag to Gitlab, effectively creating a new release, a complete CI pipeline run will start which will push a tagged Docker image to the registry, update the :latest image and try and deploy the app to Google Kubernetes Engine.

### Performance

Latest Lighthouse performance report, created by CI/CD pipeline, will always be found in https://gomezortiz.gitlab.io/vue-task-manager/lighthouse/latest.html