deploy-major:
		@npm version major -m "New tagged version: %s"

deploy-minor:
		@npm version minor -m "New tagged version: %s"

deploy-patch:
		@npm version patch -m "New tagged version: %s"

current:
		@node -p "require('./package.json').version"

start:
		@npm run serve

build:
		@npm run build

test:
		@npm run test:unit

lint:
		@npm run lint

.PHONY: deploy-major deploy-minor deploy-patch current start build test lint